package by.zwezh.trees;

public class ZBinarySearchTree {
    private ZTreeNode root;

    public ZBinarySearchTree() {
        this.root = null;
    }

    public ZTreeNode getRoot() {
        return root;
    }

    public ZTreeNode create(int value) {
        return new ZTreeNode(value, null, null);
    }

    public void add(ZTreeNode start, ZTreeNode newNode) {
        if (root == null) {
            root = newNode;
            return;
        }
        if (newNode.getValue() > start.getValue()) {
            if (start.getRight() == null) {
                start.setRight(newNode);
            }
            add(start.getRight(), newNode);
        }
        if (newNode.getValue() < start.getValue()) {
            if (start.getLeft() == null) {
                start.setLeft(newNode);
            }
            add(start.getLeft(), newNode);
        }
    }

    public void search(int value, ZTreeNode start) {
        if (start == null) {
            System.out.println("Node isn't found");
        }
        if (value == start.getValue()) {
            System.out.println("Node is found");
            return;
        }
        if (value > start.getValue()) {
            search(value, start.getRight());
        }
        if (value < start.getValue()) {
            search(value, start.getLeft());
        }
    }
}
