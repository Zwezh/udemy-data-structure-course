package by.zwezh.trees;

public class ZTreeNode {
    private int value;
    private ZTreeNode left;
    private ZTreeNode right;

    public ZTreeNode(int value, ZTreeNode left, ZTreeNode right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public ZTreeNode getLeft() {
        return left;
    }

    public void setLeft(ZTreeNode left) {
        this.left = left;
    }

    public ZTreeNode getRight() {
        return right;
    }

    public void setRight(ZTreeNode right) {
        this.right = right;
    }
}
