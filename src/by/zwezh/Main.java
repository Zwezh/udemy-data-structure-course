package by.zwezh;

import by.zwezh.demo.*;
import by.zwezh.graph.ZShortPath;
import by.zwezh.searching.*;
import by.zwezh.sorting.*;

public class Main {

    public static void main(String[] args) {
//        new DynamicArrayDemo();
//        new ZLinkedListDemo();
//        new ZLinkedDoubleListDemo();
//        new ZHashTableDemo();
//        new ZStackDemo();
//        new ZStairsDemo();
//        new ZDynamicStackDemo();
//        new ZLinkedListStackDemo();
//        new ZQueueDemo();
//        new ZDynamicQueueDemo();
//        new ZLinkedListQueueDemo();
//        new ZLinearSearch();
//        new ZBinearSearch();
//        new ZInterpolationSearch();
//        new ZBubleSorting();
//        new ZSelectionSort();
//        new ZQuickSorting();
//        new ZMergeSorting();
//        new ZHeapSorting();
//        new ZBinarySearchTreeDemo();
//        new ZDFSGraphDemo();
//        new ZBFSGraphDemo();
        new ZShortPath();
    }
}