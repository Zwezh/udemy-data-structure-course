package by.zwezh.cases;

import java.util.ArrayList;

public class ZStair {
    public int number;
    public ArrayList<Integer> visited;

    public ZStair(int number, ArrayList<Integer> visited) {
        this.number = number;
        this.visited = new ArrayList<Integer>(visited);
        this.visited.add(this.number);
    }
}
