package by.zwezh.sorting;

import java.util.Arrays;

public abstract class ZBaseSorting {
    public ZBaseSorting(String type) {
        Integer[] arr = {1,89,43,23,56,34,23,65,24,76,23,656,4321,3,467,34,2,42,4,6,34};
        System.out.println("Before "+ type +" sort");
        System.out.println(Arrays.toString(arr));
        sorting(arr);
        System.out.println("After "+ type +" sort");
        System.out.println(Arrays.toString(arr));
    }

    protected abstract void sorting(Integer[] array);
}
