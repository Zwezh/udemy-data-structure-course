package by.zwezh.sorting;

public class ZQuickSorting extends ZBaseSorting {
    public ZQuickSorting() {
        super("Quick");
    }

    protected void sorting(Integer[] array) {
    quickSort(array,0, array.length - 1);
    }

    private void quickSort(Integer[] array, int low, int high) {
        if (low > high) {
            return;
        }
        int mid = low + (high - low) / 2;
        int pivot = array[mid];
        int i = low;
        int j = high;
        while (i <= j) {
            while ((array[i] < pivot)) {
                i++;
            }
            while (array[j] > pivot) {
                j--;
            }
            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }
        if (low < j) {
            quickSort(array, low, j);
        }
        if (high > i) {
            quickSort(array, i, high);
        }
    }
}
