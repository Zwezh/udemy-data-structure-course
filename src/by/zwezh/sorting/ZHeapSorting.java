package by.zwezh.sorting;

public class ZHeapSorting extends ZBaseSorting {

    private int total;

    public ZHeapSorting() {
        super("Heap");
    }

    protected void sorting(Integer[] array) {
        total = array.length - 1;
        for (int i = total / 2; i >= 0; i--) {
            heapify(array, i);
        }
        for (int i = total; i > 0; i--) {
            swap(array, 0, i);
            total--;
            heapify(array, 0);
        }
    }

    private void heapify(Comparable[] arr, int i) {
        int left = i * 2;
        int right = (i * 2) + 1;
        int grt = i;
        if (left <= total && arr[left].compareTo(arr[grt]) >= 0) {
            grt = left;
        }
        if (right <= total && arr[right].compareTo(arr[grt]) >= 0) {
            grt = right;
        }
        if (grt != i) {
            swap(arr, i, grt);
            heapify(arr, grt);
        }
    }

    private void swap(Comparable[] array, int a, int b) {
        Comparable temp = array[a];
        array[a] = array[b];
        array[b] = temp;
    }
}
