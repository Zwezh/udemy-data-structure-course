package by.zwezh.sorting;

public class ZMergeSorting extends ZBaseSorting {
    private Integer[] array;
    private int[] tempArray;

    public ZMergeSorting() {
        super("Merge");
    }

    protected void sorting(Integer[] arr) {
        prepareForSorting(arr);
    }

    private void prepareForSorting(Integer[] arr) {
        this.array = arr;
        this.tempArray = new int[arr.length];
        doMergeSort(0, arr.length - 1);

    }

    private void doMergeSort(int lowIndex, int highIndex) {
        if (lowIndex < highIndex) {
            int middle = lowIndex + (highIndex - lowIndex) / 2;
            doMergeSort(lowIndex, middle);
            doMergeSort(middle + 1, highIndex);
            mergePart(lowIndex, middle, highIndex);
        }
    }

    private void mergePart(int lowIndex, int middle, int highIndex) {
        for (int i = lowIndex; i <= highIndex; i++) {
            tempArray[i] = array[i];
        }
        int i = lowIndex;
        int j = middle + 1;
        int k = lowIndex;
        while (i <= middle && j <= highIndex) {
            if (tempArray[i] <= tempArray[j]) {
                array[k] = tempArray[i];
                i++;
            } else {
                array[k] = tempArray[j];
                j++;
            }
            k++;
        }
        while (i <= middle) {
            array[k] = tempArray[i];
            k++;
            i++;
        }
    }
}
