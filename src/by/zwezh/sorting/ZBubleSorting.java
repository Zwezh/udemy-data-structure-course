package by.zwezh.sorting;

import java.util.Arrays;

public class ZBubleSorting extends ZBaseSorting {
    public ZBubleSorting() {
        super("Bubble");
    }

    protected void sorting(Integer[] array) {
        int size = array.length;
        int temp = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 1; j < (size - i); j++) {
                if (array[j - 1] > array[j]) {
                    temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                }
            }
        }
    }
}
