package by.zwezh.sorting;

public class ZSelectionSort extends ZBaseSorting {
    public ZSelectionSort() {
        super("Selection");
    }

    protected void sorting(Integer[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            int index = i;
            for (int j = i+1; j < array.length; j++) {
                if(array[j] < array[index]){
                    index = j;
                }
            }
            if(index!=i) {
                int temp = array[index];
                array[index]=array[i];
                array[i]=temp;
            }
        }
    }
}
