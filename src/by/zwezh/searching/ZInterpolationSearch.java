package by.zwezh.searching;

public class ZInterpolationSearch {
    public ZInterpolationSearch() {
        ZDataSet data = new ZDataSet(1000000);
        int search = 12500;
        boolean isFound = false;
        int low = 0;
        int high = data.getSize() - 1;
        int mid = 0;
        while (!isFound) {
            if (low > high) {
                System.out.println("The number " + search + " wasn't found, after " + data.numberTry + " tries");
                break;
            }
            mid = low + ((high - low) / (data.getData()[high] - data.getData()[low]))*(search - data.getData()[low]);
            data.numberTry++;
            if (data.getData()[mid] == search) {
                isFound = true;
                System.out.println("Element is found after " + data.numberTry + " try ");
                break;
            }
            if (data.getData()[mid] < search) {
                low = mid + 1;
            }
            if (data.getData()[mid] > search) {
                high = mid - 1;
            }
        }
    }
}
