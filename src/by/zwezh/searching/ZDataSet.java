package by.zwezh.searching;

public class ZDataSet {
    private int[] data;
    int numberTry;

    public ZDataSet(int size) {
        data = new int[size];
        for (int i = 1; i <= size; i++) {
            data[i - 1] = i;
        }
        numberTry = 0;
    }

    public int[] getData() {
        return data;
    }

    public int getSize() {
        return data.length;
    }
}
