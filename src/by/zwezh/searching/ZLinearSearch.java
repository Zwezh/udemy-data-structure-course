package by.zwezh.searching;

public class ZLinearSearch {

    public ZLinearSearch() {
        ZDataSet data = new ZDataSet(1000000);
        int search = 7111100;
        boolean isFound = false;
        for (int i = 0; i < data.getSize(); i++) {
            data.numberTry++;
            if (data.getData()[i] == search) {
                isFound = true;
                System.out.println("Element is found after " + data.numberTry + " try ");
                break;
            }
        }
        if (!isFound) {
            System.out.println("The number " + search + " wasn't found");
        }
    }
}