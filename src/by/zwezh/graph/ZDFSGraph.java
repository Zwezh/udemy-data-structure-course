package by.zwezh.graph;

import java.util.Stack;

public class ZDFSGraph extends ZBaseGraph {

    public ZDFSGraph(int size) {
        super(size);
    }

    public void DFSExplore(int startVertex) {
        Boolean[] visited = new Boolean[size];
        for (int i = 0; i < size; i++) {
            visited[i] = false;
        }
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(startVertex);
        while (!stack.isEmpty()) {
            int n = stack.pop();
            stack.push(n);
            visited[n] = true;
            ZGraphNode head = array[n].head;
            Boolean isDone = true;
            while (head != null) {
                if (visited[head.value] == false) {
                    stack.push(head.value);
                    visited[head.value] = true;
                    isDone = false;
                    break;
                } else {
                    head = head.next;
                }
            }
            if (isDone == true) {
                int out = stack.pop();
                System.out.println("Visited node: " + out);
            }
        }
    }
}
