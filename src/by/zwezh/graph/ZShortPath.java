package by.zwezh.graph;

import java.util.LinkedList;
import java.util.Queue;

public class ZShortPath {
    public ZShortPath() {
        int[][] grid = {{1, 1, 1, 1, 1}, {0, 0, 0, 0, 1}, {1, 1, 1, 1, 1}, {1, 0, 0, 0, 0},{1, 1, 1, 0, 0}, {0, 0, 1, 9, 0}};
        Queue<ZGraphCell> queue = new LinkedList<ZGraphCell>();
        printArray(grid);
        move(grid, queue, 0, 0, 0);
        while (!queue.isEmpty()) {
            ZGraphCell bottomCell = queue.poll();
            move(grid, queue, bottomCell.rowIndex - 1, bottomCell.colIndex, bottomCell.distance + 1);
            move(grid, queue, bottomCell.rowIndex + 1, bottomCell.colIndex, bottomCell.distance + 1);
            move(grid, queue, bottomCell.rowIndex, bottomCell.colIndex - 1, bottomCell.distance + 1);
            move(grid, queue, bottomCell.rowIndex, bottomCell.colIndex + 1, bottomCell.distance + 1);
        }
    }

    private void move(int[][] grid, Queue<ZGraphCell> queue, int row, int col, int distance) {
        if (row >= 0 && row < grid.length && col >= 0 && col < grid[0].length) {
            if (grid[row][col] == 1) {
                queue.add(new ZGraphCell(row, col, distance));
                grid[row][col] = -1;
            } else if (grid[row][col] == 9) {
                System.out.println("Path is found. Distance: " + distance);
                System.exit(0);
            }
        }
    }

    private void printArray(int[][] arr) {
        for (int row = 0; row < arr.length; row++)//Cycles through rows
        {
            for (int col = 0; col < arr[row].length; col++)//Cycles through columns
            {
                System.out.printf("%5d", arr[row][col]); //change the %5d to however much space you want
            }
            System.out.println(); //Makes a new row
        }
    }
}
