package by.zwezh.graph;

public class ZGraphNode {
    int value;
    ZGraphNode next;

    public ZGraphNode(int value, ZGraphNode next) {
        this.value = value;
        this.next = next;
    }
}
