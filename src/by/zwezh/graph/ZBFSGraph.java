package by.zwezh.graph;

import java.util.LinkedList;
import java.util.Queue;

public class ZBFSGraph extends ZBaseGraph {

    public ZBFSGraph(int size) {
        super(size);
    }

    public void BFSExplore(int startVertex) {
        Boolean[] visited = new Boolean[size];
        for (int i = 0; i < size; i++) {
            visited[i] = false;
        }
        Queue<Integer> queue = new LinkedList<Integer>();
        queue.add(startVertex);
        while (!queue.isEmpty()) {
            int n = queue.poll();
            System.out.println("Visited node: " + n);
            visited[n] = true;
            ZGraphNode head = array[n].head;
            while (head != null) {
                if (visited[head.value] == false) {
                    queue.add(head.value);
                    visited[head.value] = true;
                } else {
                    head = head.next;
                }
            }
        }
    }
}
