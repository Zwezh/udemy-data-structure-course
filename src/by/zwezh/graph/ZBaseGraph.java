package by.zwezh.graph;

public class ZBaseGraph {
    protected int size;
    protected ZAdjList[] array;

    public ZBaseGraph(int size) {
        this.size = size;
        array = new ZAdjList[size];
        for (int i = 0; i < size; i++) {
            array[i] = new ZAdjList();
            array[i].head = null;
        }
    }

    public void add(int src, int dest) {
        ZGraphNode n = new ZGraphNode(dest, null);
        n.next = array[src].head;
        array[src].head = n;
    }
}
