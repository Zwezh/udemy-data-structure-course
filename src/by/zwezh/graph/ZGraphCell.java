package by.zwezh.graph;

public class ZGraphCell {
    public int rowIndex;
    public int colIndex;
    public int distance;

    public ZGraphCell(int rowIndex, int colIndex, int distance) {
        this.rowIndex = rowIndex;
        this.colIndex = colIndex;
        this.distance = distance;
    }
}
