package by.zwezh.implementation;

public class ZQueue<T> {
    private Object[] queue;
    private int rear;
    private int front;
    private int size;

    public ZQueue(int size) {
        this.size = size;
        this.queue = new Object[size];
        front = -1;
        rear = -1;
    }

    public Boolean isFull() {
        return (rear == size - 1);
    }

    public Boolean isEmpty() {
        return (front == -1 || front > rear);
    }

    public void in(T newItem) {
        if (isFull()) {
            System.out.println("The queue is full");
            return;
        }
        rear++;
        queue[rear] = newItem;
        if (front == -1) {
            front = 0;
        }
    }

    public T out() {
        if (isEmpty()) {
            System.out.println("The queue is empty");
        return null;
        }
        T outElement = (T) queue[front];
        front++;
        return outElement;
    }
}
