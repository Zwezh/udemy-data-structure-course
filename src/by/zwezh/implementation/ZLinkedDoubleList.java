package by.zwezh.implementation;

public class ZLinkedDoubleList<T> {
    NodeDouble head;

    public ZLinkedDoubleList() {
        head = null;
    }

    public void add(T value) {
        NodeDouble newNode = new NodeDouble(value, null, null);
        if (head == null) {
            head = newNode;
        } else {
            newNode.setNext(head);
            head.setPrevious(newNode);
            head = newNode;
        }
    }

    public void delete() {
        head = head.getNext();
        head.setPrevious(null);
    }

    public void display() {
        NodeDouble node = head;
        while (node != null) {
            System.out.println(node.getValue());
            node = node.getNext();
        }
    }
}
