package by.zwezh.implementation;

public class ZLinkedListStack<T> {

    Node top;

    public ZLinkedListStack() {
        top = null;
    }

    public void push(T value) {
        Node newNode = new Node(value, null);
        if (top == null) {
            top = newNode;
        } else {
            newNode.setNext(top);
            top = newNode;
        }
    }

    public T pop() {
        if (top == null) {
            System.out.println("Stack is empty");
            return null;
        }
        T value = (T) top.value;
        top = top.getNext();
        return value;
    }

    public void display() {
        Node node = top;
        while (node != null) {
            System.out.println(node.getValue());
            node = node.getNext();
        }
    }
}
