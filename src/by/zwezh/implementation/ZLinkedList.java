package by.zwezh.implementation;

public class ZLinkedList<T> {
    Node head;

    public ZLinkedList() {
        head = null;
    }

    public void add(T value) {
        Node newNode = new Node(value, null);
        if (head == null) {
            head = newNode;
        } else {
            newNode.setNext(head);
            head = newNode;
        }
    }

    public void delete() {
        head = head.getNext();
    }

    public void display() {
        Node node = head;
        while (node != null) {
            System.out.println(node.getValue());
            node = node.getNext();
        }
    }
}
