package by.zwezh.implementation;

public class ZStack<T> {

    private Object[] stack;
    private int size;
    private int top;

    public ZStack(int size) {
        this.size = size;
        stack = new Object[size];
        top = -1;
    }

    public void push(T item) {
        if (isFull()) {
            System.out.println("Stack is full");
            return;
        }
        top++;
        stack[top] = item;
    }

    public T pop() {
        if (isEmpty()) {
            System.out.println("Stack is empty");
            return null;
        }
        T item = (T) stack[top];
        top--;
        return item;
    }

    public Boolean isFull() {
        return top == (size - 1);
    }

    public Boolean isEmpty() {
        return top == -1;
    }
}
