package by.zwezh.implementation;

import java.util.Arrays;

public class ZDynamicQueue<T> {
    private Object[] queue;
    private int rear;
    private int front;
    private int size;

    public ZDynamicQueue(int size) {
        this.size = size;
        this.queue = new Object[size];
        front = -1;
        rear = -1;
    }

    public Boolean isEmpty() {
        return (front == -1 || front > rear);
    }

    public void in(T newItem) {
        ensureCapacity(rear + 2);
        rear++;
        queue[rear] = newItem;
        if (front == -1) {
            front = 0;
        }
    }

    public T out() {
        if (isEmpty()) {
            System.out.println("The queue is empty");
            return null;
        }
        T outElement = (T) queue[front];
        front++;
        return outElement;
    }

    public int getSize() {
        return queue.length;
    }

    private void ensureCapacity(int minCapacity) {
        int oldCapacity = getSize();
        if (minCapacity > oldCapacity) {
            int newCapacity = oldCapacity * 2;
            if (newCapacity < minCapacity) {
                newCapacity = minCapacity;
            }
            queue = Arrays.copyOf(queue, newCapacity);
        }
    }
}
