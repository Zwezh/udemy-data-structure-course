package by.zwezh.implementation;

public class ZLinkedListQueue<T> {
    private NodeDouble rear;
    private NodeDouble front;

    public ZLinkedListQueue() {
        front = null;
        rear = null;
    }

    public void in(T value) {
        NodeDouble newNode = new NodeDouble(value, null, null);
        if (rear == null) {
            rear = newNode;
            front = newNode;
        } else {
            newNode.setNext(null);
            newNode.setPrevious(rear);
            rear.setNext(newNode);
            rear = newNode;
        }
    }

    public T out() {
        if (rear == null || front == null) {
            System.out.println("The queue is empty");
            return null;
        }
        T value = (T) front.getValue();
        front = front.getNext();
        if (front != null) {
            front.setPrevious(null);
        }
        return value;
    }
}
