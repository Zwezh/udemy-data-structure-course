package by.zwezh.implementation;

public class ZEntry {

    private int key;
    private Object value;
    private ZEntry next;

    public ZEntry() {
        this.next = null;
    }

    public ZEntry(int key, Object value) {
        this.setKey(key);
        this.setValue(value);
        this.next = null;
    }

    public ZEntry getNext() {
        return next;
    }

    public void setNext(ZEntry next) {
        this.next = next;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
