package by.zwezh.implementation;

import java.util.Arrays;

public class ZDynamicStack<T> {

    private Object[] stack;
    private int top;

    public ZDynamicStack(int size) {
        stack = new Object[size];
        top = -1;
    }

    public void push(T item) {
        ensureCapacity(top + 2);
        top++;
        stack[top] = item;
    }

    public T pop() {
        if (isEmpty()) {
            System.out.println("Stack is empty");
            return null;
        }
        T item = (T) stack[top];
        top--;
        return item;
    }

    public Boolean isEmpty() {
        return top == -1;
    }

    public int getSize() {
        return stack.length;
    }

    private void ensureCapacity(int minCapacity) {
        int oldCapacity = getSize();
        if (minCapacity > oldCapacity) {
            int newCapacity = oldCapacity * 2;
            if (minCapacity < newCapacity) {
                newCapacity = minCapacity;
            }
            stack = Arrays.copyOf(stack, newCapacity);
        }
    }
}
