package by.zwezh.implementation;

public class NodeDouble {
    private Object value;
    private NodeDouble next;
    private NodeDouble previous;

    public NodeDouble(Object value, NodeDouble next, NodeDouble previous) {
        this.setValue(value);
        this.setNext(next);
        this.setPrevious(previous);
    }


    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public NodeDouble getNext() {
        return next;
    }

    public void setNext(NodeDouble next) {
        this.next = next;
    }

    public NodeDouble getPrevious() {
        return previous;
    }

    public void setPrevious(NodeDouble previous) {
        this.previous = previous;
    }
}
