package by.zwezh.implementation;

public class ZHashTable<T> {
    private ZEntry[] array;
    private int size;

    public ZHashTable(int size) {
        this.size = size;
        array = new ZEntry[size];
        for (int i = 0; i < size; i++) {
            array[i] = new ZEntry();
        }
    }

    public void put(int key, Object value) {
        int hashIndex = getHash(key);
        ZEntry hashValue = array[hashIndex];
        ZEntry newItem = new ZEntry(key, value);
        newItem.setNext(hashValue.getNext());
        hashValue.setNext(newItem);
    }

    public T get(int key) {
        T value = null;
        int hashIndex = getHash(key);
        ZEntry hashValue = array[hashIndex];
        while (hashValue != null) {
            if (hashValue.getKey() == key) {
                value = (T) hashValue.getValue();
                break;
            }
            hashValue = hashValue.getNext();
        }
        return value;
    }

    private int getHash(int key) {
        return key % size;
    }
}
