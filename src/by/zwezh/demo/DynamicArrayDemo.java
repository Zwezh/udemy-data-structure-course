package by.zwezh.demo;

import by.zwezh.implementation.DynamicArray;

public class DynamicArrayDemo {
    public DynamicArrayDemo() {
        DynamicArray<Integer> array = new DynamicArray<>();
        array.put(12);
        System.out.println(array.getSize());
        array.put(1);
        System.out.println(array.getSize());
        array.put(13);
        System.out.println(array.getSize());
        array.put(123);
        System.out.println(array.getSize());
        array.put(11);
        System.out.println(array.getSize());
        array.put(1123);
        System.out.println(array.getSize());
        array.put(1223);
        System.out.println(array.getSize());
        array.put(1233);
        System.out.println(array.getSize());
        System.out.println("Dynamic array items :");
        for (int i = 0; i < array.getSize(); i++) {
            System.out.println(array.get(i));
        }
    }
}
