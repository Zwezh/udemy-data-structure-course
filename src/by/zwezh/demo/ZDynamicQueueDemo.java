package by.zwezh.demo;

import by.zwezh.implementation.ZDynamicQueue;

public class ZDynamicQueueDemo {
    public ZDynamicQueueDemo() {
        ZDynamicQueue<Integer> queue = new ZDynamicQueue<Integer>(6);
        queue.in(1);
        System.out.println(queue.out());
        System.out.println(queue.out());
        System.out.println(queue.out());
        queue.in(2);
        System.out.println("Size: " + queue.getSize());
        System.out.println(queue.out());
        queue.in(3);
        System.out.println("Size: " + queue.getSize());
        queue.in(4);
        System.out.println("Size: " + queue.getSize());
        System.out.println("Size: " + queue.getSize());
        queue.in(5);
        System.out.println("Size: " + queue.getSize());
        queue.in(6);
        System.out.println("Size: " + queue.getSize());
        queue.in(7);
        System.out.println("Size: " + queue.getSize());
        queue.in(8);
        System.out.println("Size: " + queue.getSize());
        queue.in(9);
        System.out.println("Size: " + queue.getSize());
        queue.in(10);
        System.out.println("Size: " + queue.getSize());
        queue.in(11);
        System.out.println(queue.out());
        System.out.println(queue.out());
    }
}
