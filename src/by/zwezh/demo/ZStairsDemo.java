package by.zwezh.demo;

import by.zwezh.cases.ZStair;

import java.util.ArrayList;
import java.util.Stack;

public class ZStairsDemo {

    public ZStairsDemo() {
        int step = 9;
        Stack<ZStair> stairStack = new Stack<ZStair>();
        stairStack.add(new ZStair(0, new ArrayList<Integer>()));
        while (!stairStack.isEmpty()) {
            ZStair zStair = stairStack.pop();
            if (zStair.number == step) {
                System.out.println(zStair.visited);
                continue;
            }
            int oneStair = zStair.number + 1;
            if (oneStair <= step) {
                stairStack.add(new ZStair(oneStair, zStair.visited));

            }
            int twoStair = zStair.number + 2;
            if (twoStair <= step) {
                stairStack.add(new ZStair(twoStair, zStair.visited));

            }
        }
    }
}
