package by.zwezh.demo;

import by.zwezh.implementation.ZDynamicStack;

public class ZDynamicStackDemo {
    public ZDynamicStackDemo() {
        ZDynamicStack<Integer> stack = new ZDynamicStack<Integer>(2);
        stack.push(1);
        stack.push(2);
        stack.push(3);
        System.out.println("Size: " + stack.getSize());
        stack.push(4);
        stack.push(5);
        System.out.println("Size: " + stack.getSize());
        stack.push(6);
        System.out.println("Size: " + stack.getSize());
        stack.push(7);
        System.out.println("Size: " + stack.getSize());
        stack.push(8);
        System.out.println("Size: " + stack.getSize());
        stack.push(9);
        System.out.println("Size: " + stack.getSize());
        stack.push(10);
        System.out.println("Size: " + stack.getSize());
        stack.push(11);
        System.out.println("Size: " + stack.getSize());
        stack.push(19);
        while (!stack.isEmpty()) {
            System.out.println(stack.pop());
        }
        System.out.println(stack.pop());
    }
}
