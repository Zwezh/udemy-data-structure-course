package by.zwezh.demo;

import by.zwezh.graph.ZDFSGraph;

public class ZDFSGraphDemo {
    public ZDFSGraphDemo() {
        ZDFSGraph graph = new ZDFSGraph(6);
        graph.add(0,2);
        graph.add(0,1);
        graph.add(1,4);
        graph.add(1,3);
        graph.add(1,2);
        graph.add(3,1);
        graph.add(4,1);
        graph.add(2,5);
        graph.add(2,2);
        graph.add(5,2);
        graph.DFSExplore(1);
    }
}
