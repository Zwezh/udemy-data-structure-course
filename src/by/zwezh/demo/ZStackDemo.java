package by.zwezh.demo;

import by.zwezh.implementation.ZStack;

public class ZStackDemo {
    public ZStackDemo() {
        ZStack<Integer> stack = new ZStack<Integer>(7);
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        stack.push(6);
        stack.push(7);
        stack.push(8);
        stack.push(9);
        while (!stack.isEmpty()) {
            System.out.println(stack.pop());
        }
        System.out.println(stack.pop());
    }
}
