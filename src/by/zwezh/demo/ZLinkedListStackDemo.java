package by.zwezh.demo;

import by.zwezh.implementation.ZLinkedListStack;

public class ZLinkedListStackDemo {
    public ZLinkedListStackDemo() {
        ZLinkedListStack<Integer> stack = new ZLinkedListStack<Integer>();
stack.pop();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        stack.push(6);
        stack.push(7);
        stack.push(8);
        stack.push(9);
        stack.push(10);
        stack.push(11);
        stack.push(19);
        System.out.println(stack.pop());
    }
}
