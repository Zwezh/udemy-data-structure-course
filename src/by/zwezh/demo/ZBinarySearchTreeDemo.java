package by.zwezh.demo;

import by.zwezh.trees.ZBinarySearchTree;

public class ZBinarySearchTreeDemo {
    public ZBinarySearchTreeDemo() {
        ZBinarySearchTree tree = new ZBinarySearchTree();
        tree.add(tree.getRoot(), tree.create(10));
        tree.add(tree.getRoot(), tree.create(12));
        tree.add(tree.getRoot(), tree.create(11));
        tree.add(tree.getRoot(), tree.create(13));
        tree.add(tree.getRoot(), tree.create(14));
        tree.add(tree.getRoot(), tree.create(17));
        tree.add(tree.getRoot(), tree.create(16));
        tree.add(tree.getRoot(), tree.create(15));
        tree.search(14, tree.getRoot());
    }
}
