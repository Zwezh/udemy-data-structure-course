package by.zwezh.demo;

import by.zwezh.implementation.ZQueue;

public class ZQueueDemo {
    public ZQueueDemo() {
        ZQueue<Integer> queue = new ZQueue<Integer>(6);
        queue.in(1);
        System.out.println(queue.out());
        System.out.println(queue.out());
        System.out.println(queue.out());
        queue.in(2);
        System.out.println(queue.out());
        queue.in(3);
        queue.in(4);
        queue.in(5);
        queue.in(6);
        queue.in(7);
        queue.in(8);
        queue.in(9);
        queue.in(10);
        queue.in(11);
        System.out.println(queue.out());
        System.out.println(queue.out());
    }
}
