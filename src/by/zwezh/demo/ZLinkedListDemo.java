package by.zwezh.demo;

import by.zwezh.implementation.ZLinkedList;

public class ZLinkedListDemo {
    public ZLinkedListDemo() {
        ZLinkedList<String> list = new ZLinkedList<>();
        list.add("One");
        list.add("Two");
        list.add("Three");
        list.add("Four");
        list.add("Five");
        list.add("Six");
        list.add("Eight");
        list.add("Nine");
        list.add("Ten");
        list.display();
        list.delete();
        System.out.println("After deleting 1 items");
        list.display();
        list.delete();
        list.delete();
        list.delete();
        System.out.println("After second deleting 3 items");
        list.display();
    }
}
