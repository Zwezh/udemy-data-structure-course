package by.zwezh.demo;

import by.zwezh.graph.ZBFSGraph;

public class ZBFSGraphDemo {
    public ZBFSGraphDemo() {
        ZBFSGraph graph = new ZBFSGraph(6);
        graph.add(0,2);
        graph.add(0,1);
        graph.add(1,4);
        graph.add(1,3);
        graph.add(1,2);
        graph.add(3,1);
        graph.add(4,1);
        graph.add(2,5);
        graph.add(2,2);
        graph.add(5,2);
        graph.BFSExplore(1);
    }

}
