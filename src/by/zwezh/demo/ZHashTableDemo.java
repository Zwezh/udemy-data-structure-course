package by.zwezh.demo;

import by.zwezh.implementation.ZHashTable;

public class ZHashTableDemo {
    public ZHashTableDemo() {
        ZHashTable<String> hash = new ZHashTable<String>(10);
        hash.put(11, "One");
        hash.put(12, "Two");
        hash.put(13, "Three");
        hash.put(14, "Four");
        hash.put(15, "Five");
        hash.put(16, "Six");
        System.out.println(hash.get(13));
        hash.put(16, "Six");
        hash.put(26, "Six 26");
        hash.put(6, "Six 0");
        System.out.println(hash.get(6));
        System.out.println(hash.get(26));
    }
}
